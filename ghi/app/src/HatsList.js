import React, { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';

function Hat(props) {
  const { hat } = props;

  const removeHat = async () => {
    await fetch(`http://localhost:8090/api/hats/${hat.id}/`, { method: "delete" });
  };

  return (
    <div key={hat.id} className="card mb-3 shadow">
      <img src={hat.picture_url} className="card-img-top" />
      <div className="card-body">
        <div className="card-title">{hat.color} {hat.style_name} by {hat.fabric}</div>
      </div>
      <div className="card-footer">
        <div className="btn" onClick={removeHat}>
          Remove Hat
        </div>
      </div>
    </div>
  );
}

function Location(props) {
  const { hats, location } = props;
  const sectionNumber = location.href[location.href.length - 2];

  return (
    <div className="col">
      <h5 className="container text-center">Location {sectionNumber}: Capacity {location.section_number}</h5>
      <div className="row">
        {hats.map(hat => <Hat hat={hat} key={hat.id} />)}
      </div>
    </div>
  );
}

function Closet(props) {
  const { closet, locations, hats } = props;

  return (
    <div className="container shadow">
      <h4 className="container text-center">{closet}</h4>
      <div className="row">
        {locations.map(location => (
          <Location hats={hats.filter(hat => hat.location === location.href)} location={location} key={location.href} />
        ))}
      </div>
    </div>
  );
}

function HatsList() {
  const [hats, setHats] = useState([]);
  const [locations, setLocations] = useState([]);
  const [closets, setClosets] = useState([]);

  const fetchHatData = async () => {
    const url = 'http://localhost:8090/api/hats/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setHats(data.hats);
    }
  };

  const fetchLocationData = async () => {
    const url = 'http://localhost:8100/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
      setClosets(
        data.locations
          .map(location => location.closet_name)
          .reduce((closet_list, current_closet) => {
            if (!closet_list.includes(current_closet)) {
              closet_list.push(current_closet);
            }
            return closet_list;
          }, [])
      );
    }
  };

  useEffect(() => {
    fetchHatData();
    fetchLocationData();
  }, []);

  return (
    <>
      <h1 className="text-center">All of your hats</h1>
      <div className="text-center">
        <NavLink className="btn" to="/hats/new">Add hat</NavLink>
      </div>
      <div className="row">
        {closets.map(closet => (
          <Closet
            closet={closet}
            locations={locations.filter(location => closet === location.closet_name)}
            hats={hats}
            key={closet}
          />
        ))}
      </div>
    </>
  );
}



export default HatsList;


