import React , { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';

function Shoe(props) {
    const { shoe } = props;
    return (<>
        <div key={shoe.id} className="card mb-3 shadow">
            <img src={shoe.picture} className="card-img-top" />
            <div className="card-body">
                <div className="card-title">{shoe.color} {shoe.model_name} by {shoe.manufacturer}</div>
            </div>
            <div className="card-footer">
                {/* <NavLink className="btn" to="/shoes/new" state={{ shoe: shoe }}>Edit shoe</NavLink> */}
                <div className="btn" onClick={async () => await fetch(`http://localhost:8080/api/shoes/${shoe.id}/`, {method: "delete"})}>
                    Remove Shoe
                </div>
            </div>
        </div>
      </>
    );
}

function Bin(props) {
    const { shoes, bin } = props;
    return (
        <div className="col">
            <h5 className="container text-center">Bin {bin.href[bin.href.length - 2]}: Capacity {bin.bin_size}</h5>
            <div className="row">
                {shoes.map(shoe => <Shoe shoe={shoe} key={shoe.id} />)}
            </div>
        </div>
    );
}

function Closet(props) {
    const { closet , bins , shoes } = props;
    return (
    <div className="container shadow">
        <h4 className="container text-center">{closet}</h4>
        <div className="row">
            {bins.map(bin => {
                return (
                    <Bin shoes={shoes.filter(shoe => shoe.bin === bin.href)} bin={bin} key={bin.href}/>
                );
            })}
        </div>
    </div>
    );
}

function ShoesList() {
    const [shoes, setShoes] = useState([]);
    const [bins, setBins] = useState([]);
    const [closets, setClosets] = useState([]);

    const fetchShoeData = async () => {
        const url = 'http://localhost:8080/api/shoes/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes);
        }
    }
    const fetchBinData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
            setClosets(data.bins.map(bin => bin.closet_name).reduce((closet_list, current_closet) => {
                if (!closet_list.includes(current_closet)) {
                    closet_list.push(current_closet);
                }
                return closet_list;
            }, []));
        }
    }
    useEffect(() => {fetchShoeData(); fetchBinData();}, []);

    return (<>
    <h1 className="text-center">All of your shoes</h1>
    <div className="text-center">
        <NavLink className="btn" to="/shoes/new">Add shoe</NavLink>
        {/* <NavLink className="btn" to="/shoes/new/bin">Add bin</NavLink> */}
    </div>
    <div className="row">
        {closets.map(closet => {
            return (
                <Closet closet={closet} bins={bins.filter(bin => closet === bin.closet_name)} shoes={shoes} key={closet}/>
            );
        })}
    </div>
    </>);
}

export default ShoesList;
