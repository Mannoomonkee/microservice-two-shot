import React, { useEffect, useState } from 'react';

function HatForm() {
  const [locations, setLocations] = useState([]);
  const [alertClasses, setAlertClasses] = useState("alert alert-warning d-none");
  const [hatData, setHatData] = useState({
    style_name: "",
    fabric: "",
    color: "",
    picture_url: "",
    location: ""
  });
  let method = "post";

  const handleInput = (e) => {
    setHatData({
      ...hatData,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const hatUrl = "http://localhost:8090/api/hats/";
    const fetchConfig = {
      method: method,
      body: JSON.stringify(hatData),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok) {
      const newHat = await response.json();
      if (newHat.message === "Location is exist!") {
        setAlertClasses("alert alert-warning");
      }
      setHatData({
        style_name: "",
        fabric: "",
        color: "",
        picture_url: "",
        location: ""
      });
    }
  };

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setLocations(data.bins);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Hat Data</h1>
            <form onSubmit={handleSubmit} id="hat-form">
              <div className="form-floating mb-3">
                <input
                  onChange={handleInput}
                  value={hatData.fabric}
                  placeholder=""
                  name="fabric"
                  required
                  type="text"
                  id="fabric"
                  className="form-control"
                />
                <label htmlFor="fabric">Fabric name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleInput}
                  value={hatData.style_name}
                  placeholder=""
                  name="style_name"
                  required
                  type="text"
                  id="style_name"
                  className="form-control"
                />
                <label htmlFor="style_name">Style name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleInput}
                  value={hatData.color}
                  placeholder=""
                  name="color"
                  required
                  type="text"
                  id="color"
                  className="form-control"
                />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleInput}
                  value={hatData.picture_url}
                  placeholder=""
                  name="picture_url"
                  required
                  type="text"
                  id="picture_url"
                  className="form-control"
                />
                <label htmlFor="picture_url">Picture</label>
              </div>
              <div className={alertClasses}>
                Location {hatData.location[hatData.location.length - 2]} is exist!
              </div>
              <div className="mb-3">
                <select
                  onChange={handleInput}
                  value={hatData.location}
                  name="location"
                  id="location"
                  className="form-select"
                  required
                >
                  <option value="">Select a Location</option>
                  {locations.map((locationObj) => {
                    if (locationObj.shelf_number) {
                      return (
                        <option
                          key={locationObj.href}
                          value={locationObj.href}
                        >
                          {locationObj.closet_name}: Location {locationObj.section_number}
                        </option>
                      );
                    }
                    return null;
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Make changes</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default HatForm;

