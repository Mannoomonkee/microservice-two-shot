from django.db import models

# Create your models here.

class BinVO(models.Model):
    bin_href = models.CharField(max_length=200)
    size = models.PositiveSmallIntegerField()


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=150, blank=True)
    model_name = models.CharField(max_length=150, blank=True)
    color = models.CharField(max_length=150, blank=True)
    picture = models.URLField(max_length=150, blank=True)

    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
    )
