from .views import api_list_shoes, api_shoe_details
from django.urls import path


urlpatterns = [
    path("shoes/", api_list_shoes, name="shoes_list"),
    path("shoes/<int:id>/", api_shoe_details, name="shoe_details"),
]
